%global _empty_manifest_terminate_build 0
Name:		python-sphinx-hoverxref
Version:	0.8b1
Release:	1
Summary:	Sphinx extension to show tooltips with content embedded when hover a reference.
License:	None
URL:		https://pypi.org/project/sphinx-hoverxref/
Source0:	https://files.pythonhosted.org/packages/2a/6b/cbb0d294e7f17e595572392a1d5135726115ebf30a3f5128049e2daa8ef6/sphinx-hoverxref-0.8b1.tar.gz
BuildArch:	noarch

Requires:	(python3-sphinx>=1.8)
Requires:	(python3-sphinxcontrib-jquery)
Requires:	(python3-sphinx)
Requires:	(python3-sphinx-autoapi)
Requires:	(python3-sphinx-rtd-theme)
Requires:	(python3-sphinx-tabs==3.3.1)
Requires:	(python3-sphinx-prompt)
Requires:	(python3-sphinx-version-warning)
Requires:	(python3-sphinx-notfound-page)
Requires:	(python3-sphinx-autobuild)
Requires:	(python3-sphinxcontrib-bibtex)
Requires:	(python3-sphinxemoji)
Requires:	(python3-tox)

%description
``sphinx-hoverxref`` is a Sphinx_ extension to add tooltips on the cross references of the documentation with the content of the linked section.

%package -n python3-sphinx-hoverxref
Summary:	Sphinx extension to show tooltips with content embedded when hover a reference.
Provides:	python-sphinx-hoverxref
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pip
%description -n python3-sphinx-hoverxref
``sphinx-hoverxref`` is a Sphinx_ extension to add tooltips on the cross references of the documentation with the content of the linked section.

%package help
Summary:	Development documents and examples for sphinx-hoverxref
Provides:	python3-sphinx-hoverxref-doc
%description help
``sphinx-hoverxref`` is a Sphinx_ extension to add tooltips on the cross references of the documentation with the content of the linked section.

%prep
%autosetup -n sphinx-hoverxref-0.8b1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "\"/%h/%f.gz\"\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-sphinx-hoverxref -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Sat Sep 09 2023 Python_Bot <Python_Bot@openeuler.org> - 0.8b1-1
- Package Spec generated
